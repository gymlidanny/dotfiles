; Danny's Polybar config

[colors]
peach = #FFCD98
lime = #CADC79
salmon = #d97464
darkgrey = #2e2e2e
lightgrey = #444444
alert = #bd2c40

xcolor0 = ${xrdb:color0}
xcolor1 = ${xrdb:color1}
xcolor2 = ${xrdb:color2}
xcolor3 = ${xrdb:color3}
xcolor4 = ${xrdb:color4}
xcolor5 = ${xrdb:color5}
xcolor6 = ${xrdb:color6}
xcolor7 = ${xrdb:color7}
xcolor8 = ${xrdb:color8}
xcolor9 = ${xrdb:color9}
xcolor10 = ${xrdb:color10}
xcolor11 = ${xrdb:color11}
xcolor12 = ${xrdb:color12}
xcolor13 = ${xrdb:color13}
xcolor14 = ${xrdb:color14}
xcolor15 = ${xrdb:color15}
background = ${xrdb:background}
foreground = ${xrdb:foreground}

; Global

[global/wm]
margin-top = 5
margin-bottom = 5

[bar/main0]
monitor = ${env:MONITOR:HDMI-0}
;monitor = ${env:MONITOR:eDP1}
width = 100%
height = 24
bottom = false
offset-x = 0%
offset-y = 0%
fixed-center = true
background = ${colors.xcolor0}
foreground = ${colors.xcolor1}

overline-size = 0
overline-color = ${colors.xcolor7}
underline-size = 2
underline-color = ${colors.xcolor7}

border-bottom-size = 0
border-bottom-color = ${colors.xcolor7}
border-top-size = 0
border-top-color = ${colors.xcolor7}
border-color-foreground = ${colors.peach}

spacing = 0
padding-left = 0
padding-right = 0
module-margin-left = 0
module-margin-right = 0

; Fonts
font-0 = PowerlineSymbols:size=15;3
font-1 = xos4 Terminus:size=12;1
font-2 = FontAwesome:size=10;2
font-3 = xos4 Terminus:size=18;0
font-4 = Monospace:size=28;3
font-5 = FontAwesome:size=26;7

; System tray
tray-position = left
tray-padding = 2
tray-transparent = false
tray-background = ${colors.xcolor1}

; Module layouts
modules-left = RightArrow3 i3 RightArrow2 xwindow RightArrow1 RightStripe RightArrow1
modules-center = RightStripe date LeftStripe
modules-right = arrow1 LeftStripe arrow1 filesystem arrow2 alsa arrow3 cpu arrow2 memory arrow3 updates


[bar/main1]
inherit = bar/main0
monitor = ${env:MONITOR:DVI-I-1}
tray-position = none
modules-left = i3 RightArrow2 xwindow RightArrow1 RightStripe RightArrow1

[bar/main2]
inherit = bar/main0
monitor = ${env:MONITOR:DP-0}
tray-position = none
modules-left = i3 RightArrow2 xwindow RightArrow1 RightStripe RightArrow1

; Modules

[module/filesystem]
type = internal/fs
interval = 5
fixed-values = true

mount-0 = /
mount-1 = /mnt/fileserver

format-mounted = <label-mounted>
format-mounted-foreground = ${colors.xcolor7}
format-mounted-background = ${colors.xcolor1}
format-unmounted = <label-unmounted>
format-unmounted-foreground = ${colors.xcolor7}
format-unmounted-background = ${colors.xcolor1}

label-mounted = "%mountpoint%: %percentage_used%%"
; %free%% %percentage_used%% %used%
label-mounted-foreground = ${colors.xcolor7}
label-mounted-background = ${colors.xcolor1}
label-unmounted = %mountpoint%
label-unmounted-foreground = ${colors.xcolor7}
label-unmounted-background = ${colors.xcolor1}

[module/alsa]
type = internal/alsa

master-soundcard = default
master-mixer = Master

format-volume = <label-volume>
format-muted = <label-muted>
label-volume = VOL %percentage%%
label-muted = MUTED

format-volume-foreground = ${colors.xcolor1}
format-volume-background = ${colors.xcolor7}
format-muted-foreground = ${colors.xcolor1}
format-muted-background = ${colors.xcolor7}

[module/i3]
type = internal/i3
format = <label-state><label-mode>
index-sort = true
wrapping-scroll = false
pin-workspaces = true
enable-scroll = true

label-focused = %index%
label-focused-foreground = ${colors.xcolor1}
label-focused-background = ${colors.xcolor7}
label-focused-underline = ${colors.xcolor4}
label-focused-padding = 2
label-focused-font = 3
label-unfocused = %index%
label-unfocused-padding = 2
label-unfocused-foreground = ${colors.xcolor7}
label-unfocused-background = ${colors.xcolor1}
label-occupied = %index%
label-occupied-foreground = ${colors.xcolor1}
label-occupied-background = ${colors.xcolor7}
label-occupied-padding = 2
label-occupied-font = 3
label-urgent = %index%
label-urgent-foreground = ${colors.xcolor4}
label-urgent-background = ${colors.xcolor7}
label-urgent-padding = 2
label-urgent-font = 3
label-empty = %index%
label-empty-foreground = ${colors.xcolor0}
label-empty-background = ${colors.xcolor1}
label-empty-padding = 2
label-empty-font = 3
label-visible = %index%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}
label-mode-foreground = ${colors.xcolor7}
label-mode-background = ${colors.xcolor1}

[module/xwindow]
type = internal/xwindow

format-foreground = ${colors.xcolor7}
format-background = ${colors.xcolor1}
format-padding = 2
label = %title%
label-maxlen = 25

[module/xbacklight]
type = internal/xbacklight

format = <label>
label = Backlight %percentage%%

format-foreground = ${colors.xcolor1}
format-background = ${colors.xcolor7}
format-padding = 1

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

[module/cpu]
type = internal/cpu
interval = 2
format-prefix =
format-foreground = ${colors.xcolor7}
format-background = ${colors.xcolor1}
label = CPU %percentage%%

[module/memory]
type = internal/memory
interval = 2
format-prefix =
format-foreground = ${colors.xcolor1}
format-background = ${colors.xcolor7}
format-margin-top = -5

label = MEM %percentage_used%%

[module/date]
type = internal/date
interval = 1

date = "%Y/%m/%d"
date-alt = "%Y-%m-%d"

time = %H:%M
time-alt = " %I:%M:%S %p"

format-foreground = ${colors.xcolor7}
format-background = ${colors.xcolor1}
format-padding = 2

label = %date% %time%

[module/battery]
type = internal/battery
; To list battery & adapter: $ ls -1 /sys/class/power_supply/
battery = BAT0
adapter = AC

format-charging = <label-charging>
format-charging-foreground = ${colors.xcolor7}
format-charging-background = ${colors.xcolor1}
format-discharging = <label-discharging>
format-discharging-foreground = ${colors.xcolor7}
format-discharging-background = ${colors.xcolor1}
format-full = <label-full>
format-full-foreground = ${colors.xcolor7}
format-full-background = ${colors.xcolor1}

label-charging = "BAT %percentage%%"
label-discharging = "BAT %percentage%%"
label-discharging-foreground = ${colors.xcolor7}
label-full = "Battery Full"

; Powerline

[module/pipe]
type = custom/text
content = "|"
content-foreground = ${colors.darkgrey}
content-background = ${colors.xcolor0}
content-padding = 1

[module/LeftStripe]
type = custom/text
content = ""
content-foreground = ${colors.xcolor0}
content-background = ${colors.xcolor1}

[module/arrow1]
type = custom/text
content = ""
content-foreground = ${colors.xcolor1}
content-background = ${colors.xcolor0}

[module/arrow2]
type = custom/text
content = " "
content-foreground = ${colors.xcolor7}
content-background = ${colors.xcolor1}

[module/arrow3]
type = custom/text
content = " "
content-foreground = ${colors.xcolor1}
content-background = ${colors.xcolor7}

[module/RightStripe]
type = custom/text
content = ""
content-foreground = ${colors.xcolor0}
content-background = ${colors.xcolor1}

[module/RightArrow1]
type = custom/text
content = ""
content-foreground = ${colors.xcolor1}
content-background = ${colors.xcolor0}

[module/RightArrow2]
type = custom/text
content = ""
content-foreground = ${colors.xcolor7}
content-background = ${colors.xcolor1}

[module/RightArrow3]
type = custom/text
content = ""
content-foreground = ${colors.xcolor1}
content-background = ${colors.xcolor7}

[module/MiddleArrowLeft]
type = custom/text
content = "◥"
content-foreground = ${colors.xcolor1}
content-background = ${colors.xcolor0}
;◥ ◤◢ ◣
[module/MiddleArrowRight]
type = custom/text
content = "◢"
content-foreground = ${colors.xcolor0}
content-background = ${colors.xcolor1}

[module/updates]
type = custom/script
exec = pacman -Qu | wc -l
format = <label>
format-foreground = ${colors.xcolor7}
format-background = ${colors.xcolor1}
format-padding = 1
label = Updates: %output%
interval = 90

[settings]
screenchange-reload = true
