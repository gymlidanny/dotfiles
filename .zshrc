# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory extendedglob notify
unsetopt autocd beep nomatch
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/dholman/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Additional colors
autoload -U colors %% colors

# Additional completion options
zstyle ':completion:*' use-cache on
zstyle ':completion:*:cd:*' ignore-parents parent pwd
zstyle ':completion:*' completer _complete _match _approximate
zstyle ':completion:*match:*' original only
zstyle ':completion:*approximate:*' max-errors 1 numeric
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' force-list always
zstyle ':completion:*' squeeze-slashes true

# Vim-style history searching
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

# Aliases
alias ls="ls --color=auto"
alias pacin="sudo pacman -S"
alias pacr="sudo pacman -Rns"
alias pacup="sudo pacman -Syu;auracle sync"
alias msfconsole="sudo msfconsole -q"
alias irssi="torsocks irssi"

# Powerline
if [ $(pgrep -c powerline) -eq 0 ]; then
        powerline-daemon -q
fi

# Prompt settings
#setopt PROMPT_SUBST
#GIT_PS1_SHOWUPSTREAM=auto
#GIT_PS1_SHOWDIRTYSTATE=1
#GIT_PS1_SHOWUNTRACKEDFILES=1
#PS1='$(__git_ps1 "(%s)")$ '

# Make shell stuff behave nicely
pacsearch() {
        if [[ -z $1 ]]; then
                echo "No targets specified"
                return
        fi

        pacman -Ss $1
        auracle search $1
}

man() {
        env \
        LESS_TERMCAP_mb=$'\e[01;31m' \
        LESS_TERMCAP_md=$'\e[01;31m' \
        LESS_TERMCAP_me=$'\e[0m' \
        LESS_TERMCAP_se=$'\e[0m' \
        LESS_TERMCAP_so=$'\e[01;44;33m' \
        LESS_TERMCAP_ue=$'\e[0m' \
        LESS_TERMCAP_us=$'\e[01;32m' \
        man "$@"
}

rationalise-dot() {
        if [[ $LBUFFER = *.. ]]; then
                LBUFFER+=/..
        else
                LBUFFER+=.
        fi
}
zle -N rationalise-dot
bindkey . rationalise-dot
