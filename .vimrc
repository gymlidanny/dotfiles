" General
set nocompatible
set autoread
set hidden
set showmode
set showcmd
set number
set history=1000
set gcr=a:blinkon0
set grepprg="grep -nH $*"
let g:tex_flavor = "latex"
let g:powerline_pycmd = "py3"
set laststatus=2
set visualbell
set background=dark
set runtimepath=~/.vim,$VIM/vimfiles,$VIMRUNTIME,$VIM/vimfiles/after,~/.vim/after
set autowrite
set autoread
set ttyfast
syntax on

" Make search better
set gdefault
set ignorecase
set smartcase
set hlsearch
noremap <silent> <Leader>, :noh<cr>
set incsearch
set showmatch

" X11 clipboard support
set pastetoggle=<F2> "to preserve indentation
vnoremap <C-c> "*y
map <silent><Leader>p :set paste<CR>o<esc>"*]p:set nopaste<cr>"
map <silent><Leader><S-p> :set paste<CR>O<esc>"*]p:set nopaste<cr>"

" Disable folding
set nofoldenable

" Turn off swap files
set noswapfile
set nobackup
set nowb

" Indentation
set autoindent
set smartindent
set smarttab
set shiftwidth=8
set softtabstop=8
set tabstop=8
set expandtab
filetype plugin on
filetype indent on
noremap p p=`[<C-o>
noremap P P=`[<C-o>

" Per-project indentation
au BufRead,BufEnter ~/repositories/linux/*.{c,h} set noexpandtab

set nowrap
set linebreak

set splitbelow
set splitright

" Completion
set wildmode=list:longest
set wildmenu
set wildignore=*.o,*.obj,*~
set wildignore+=vim/backups
set wildignore+=*sass-cache*
set wildignore+=*DS_Store*
set wildignore+=vendor/cache/**
set wildignore+=log/**
set wildignore+=tmp/**
set wildignore+=*.png,*.jpeg,*.gif

" Scrolling
set scrolloff=8
set sidescrolloff=15
set sidescroll=1

" Key mapping
no <Up> <Nop>
no <Down> <Nop>
no <Left> <Nop>
no <Right> <Nop>
no <Delete> <Nop>
ino <Up> <Nop>
ino <Down> <Nop>
ino <Left> <Nop>
ino <Right> <Nop>
ino <Delete> <Nop>
vno <Up> <Nop>
vno <Down> <Nop>
vno <Left> <Nop>
vno <Right> <Nop>
vno <Delete> <Nop>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <silent> <Leader>b :TagbarToggle<CR>
nmap <Leader>s :source ~/.vimrc
nmap <Leader>v :e ~/.vimrc
nnoremap <silent> <Leader>a :LinuxCodingStyle<cr>
let mapleader = " "

" Email options
au BufRead /tmp/mutt-* set tw=72
