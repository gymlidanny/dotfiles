# Environment functions
appendpath () {
    case ":$PATH:" in
        *:"$1":*)
            ;;
        *)
            PATH="${PATH:+$PATH:}$1"
    esac
}

# Setup environment
appendpath '/home/dholman/bin'
appendpath '/home/dholman/.local/bin'
appendpath '/home/dholman/.gem/ruby/2.6.0/bin'
export PATH
export EDITOR="vim"
export SUDO_EDITOR="vim"
export SDL_VIDEO_MINIMIZE_ON_FOCUS_LOSS=0
export GPGKEY=90D8F8E2C830F6129B2FBB1133836B30AA014B82
export GPG_TTY=$(tty)
export SSH_AUTH_SOCK=${XDG_RUNTIME_DIR}/gnupg/S.gpg-agent.ssh
unset SSH_AGENT_PID

# Source outside scripts
source /usr/share/doc/pkgfile/command-not-found.zsh
source /usr/share/git/completion/git-prompt.sh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
source /usr/lib/python3.7/site-packages/powerline/bindings/zsh/powerline.zsh
source ~/.rvm/scripts/rvm
